<h5 class="mt-2">FIM</h5>
<div class="row grid-responsive">
	<div class="column ">
		<div class="card">
			<div class="card-title">
				<h3>Загрузка FIM</h3>
			</div>
			<div class="card-block">
                <form method="post" action="" enctype="multipart/form-data">
                <input name="_token" type="hidden" value="{{ csrf_token() }}">
                    <input type="file" name="fim">
                    <button type="submit">Загрузить</button>
                </form>
			</div>
		</div>
	</div>
</div>