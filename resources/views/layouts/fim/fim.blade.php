<h5 class="mt-2">FIM</h5>
<div class="row grid-responsive">
	<div class="column ">
		<div class="card">
			<div class="card-title">
				<h3>FIM</h3>
			</div>
			<div class="card-block">
				<table>
					<thead>
						<tr>
							<th>FIM</th>
							<th>Путь</th>
							<th>Действия</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($data as $que)
						<tr>
							<td>{{$que->name}}</td>
							<td>{{$que->adress}}</td>
							<td><a class="button" href="/diplom_project/public/fim/observe1/id?id={{$que->id}}">Обычный</a><a class="button" href="/diplom_project/public/fim/observe2/id?id={{$que->id}}">Оптимизированный</a><td>
                        </tr>
                    @endforeach
					</tbody>
				</table>
				<a class="button" href="/diplom_project/public/fimadd">Добавить FIM</a>
			</div>
		</div>
	</div>
</div>