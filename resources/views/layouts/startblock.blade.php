<div class="row grid-responsive">
				<div class="column page-heading">
					<div class="large-card">
						<h1>Добро пожаловать {{ Auth::user()->name }} !</h1>
						<p class="text-large">Вы вошли  в информационную систему для сбора статистики применения и оптимизации руководств по идентификации и устранению неисправностей </p>
						<p>Для дальнейшей работы выберете пункт слева.</p>
					</div>
				</div>
			</div>