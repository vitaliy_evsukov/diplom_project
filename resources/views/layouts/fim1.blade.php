<div class="row grid-responsive">
				<div class="column page-heading">
					<div class="large-card">
						<h1>PMC-S1000DBIKE-B6865-EPWG1-00_001-00_en-US</h1>
						<pre>"content": {
			"pmEntry": [
				{
                    "issueInfo": {
								"_issueNumber": "001",
								"_inWork": "00"
						},
					"dmRef": {
						"dmRefAddressItems": {
							"dmTitle": {
								"techName": "Lighting",
								"infoName": "Functional item numbers common information repository"
							},
							"issueDate": {
								"_day": "31",
								"_month": "08",
								"_year": "2016"
							}
						}
					}
				},{
					"dmRef": {
						"issueInfo": {
								"_issueNumber": "002",
								"_inWork": "00"
						},
						"dmRefAddressItems": {
							"dmTitle": {
								"techName": "Lighting",
								"infoName": "Parts common information repository"
							},
							"issueDate": {
								"_day": "31",
								"_month": "08",
								"_year": "2016"
							}
						}
					}
				},
				{
					"dmRef": {
						},
						"dmRefAddressItems": {
                            "issueInfo": {
								"_issueNumber": "003",
								"_inWork": "00"
						},
							"dmTitle": {
								"techName": "Lighting",
								"infoName": "Zones common information repository"
							},
							"issueDate": {
								"_day": "31",
								"_month": "08",
								"_year": "2016"
							}
						}
					}
				},
				{
					"dmRef": {
                        "issueInfo": {
								"_issueNumber": "004",
								"_inWork": "00"
						},
						"dmRefAddressItems": {
							"dmTitle": {
								"techName": "Lighting",
								"infoName": "Support equipment common information repository"
							},
							"issueDate": {
								"_day": "31",
								"_month": "08",
								"_year": "2016"
							}
						}
					}
				},
				{
					"dmRef": {
                        "issueInfo": {
								"_issueNumber": "005",
								"_inWork": "00"
						},
						"dmRefAddressItems": {
							"dmTitle": {
								"techName": "Wiring data",
								"infoName": "Field description"
							},
							"issueDate": {
								"_day": "31",
								"_month": "08",
								"_year": "2016"
							}
						}
					}
				},
				{
					"dmRef": {
						"issueInfo": {
								"_issueNumber": "006",
								"_inWork": "00"
						},
						"dmRefAddressItems": {
							"dmTitle": {
								"techName": "Electrical system",
								"infoName": "Description of how it is made and its function"
							},
							"issueDate": {
								"_day": "31",
								"_month": "08",
								"_year": "2016"
							}
						}
					}
				},
				{
					"dmRef": {
                        "issueInfo": {
								"_issueNumber": "007",
								"_inWork": "00"
						},
						"dmRefAddressItems": {
							"dmTitle": {
								"techName": "Wiring",
								"infoName": "Equipment lists"
							},
							"issueDate": {
								"_day": "31",
								"_month": "08",
								"_year": "2016"
							}
						}
					}
				},
				{
					"dmRef": {
							"issueInfo": {
								"_issueNumber": "008",
								"_inWork": "00"
						},
						"dmRefAddressItems": {
							"dmTitle": {
								"techName": "Wiring",
								"infoName": "Wire list"
							},
							"issueDate": {
								"_day": "31",
								"_month": "08",
								"_year": "2016"
							}
						}
					}
				},
				{
					"dmRef": {
							"issueInfo": {
								"_issueNumber": "009",
								"_inWork": "00"
						},
						"dmRefAddressItems": {
							"dmTitle": {
								"techName": "Wiring",
								"infoName": "Loom list"
							},
							"issueDate": {
								"_day": "31",
								"_month": "08",
								"_year": "2016"
							}
						}
					}
                },]
				</pre>
					</div>
				</div>
			</div>