			<div class="row grid-responsive">
				<div class="column ">
					<div class="card">
						<div class="card-title">
							<h3>Список самолётов</h3>
						</div>
						<div class="card-block">
							<table>
								<thead>
									<tr>
										<th>Серия борта</th>
										<th>Номер борта</th>
                                        <th>Компания владелец</th>
                                        <th>Действие</th>
									</tr>
								</thead>
								<tbody>@foreach ($data as $que)
									<tr>
										<td>{{$que->series}}</td>
										<td>{{$que->number}}</td>
                                        <td>{{$que->company_id}}</td>
                                        <td><a class="button" href="/diplom_project/public/airplane/edit/id?id={{$que->id}}">Редактировать</a><td>
									</tr>
									@endforeach
								</tbody>
                            </table>
							
                            <div class="column">
					<!-- Default Button -->
                    <a class="button" href="/diplom_project/public/airplane/create">Добавить борт</a>
                            </div>
						</div>
					</div>
				</div>
			</div>