<!--Forms-->
			<div class="row grid-responsive">
				<div class="column ">
					<div class="card">
						<div class="card-title">
							<h3>Редактирование информации</h3>
						</div>
						<div class="card-block">
						<script>
						function OnSubmitForm() {
						if(document.pressed == 'Изменить') {
						document.myform.action ="?id={{$data->id}}";
						} else if(document.pressed == 'Удалить') {
						document.myform.action ="/diplom_project/public/airplane/delete/id?id={{$data->id}}";
						}
						return true;
						}
						</script>
							<form name="myform" method="POST" onsubmit="return OnSubmitForm();">
							<fieldset>
                                <input name="_token" type="hidden" value="{{ csrf_token() }}" />
                                <label for="nameField">Серия борта</label>
                                    <input type="text" value="{{$data->series}}" required name="series" placeholder="Серия борта" id="nameField">
                                    <label for="nameField">Номер борта</label>
									<input type="text" value="{{$data->number}}" required name="number" placeholder="Номер борта" id="nameField">
									<label for="компания">Компания</label>
									<select id="компания" name="company_id">
									@foreach ($company as $que)
									<option value="{{$que->id}}">{{$que->name}}</option>
									@endforeach
									</select>
									<br>
									<input onClick="document.pressed=this.value" class="button-primary" type="submit" value="Изменить">
                           			<button onClick="document.pressed=this.value" class="button button-outline" value="Удалить">Удалить</button
                                </fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>