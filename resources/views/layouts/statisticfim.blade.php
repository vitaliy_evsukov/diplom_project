<h5 class="mt-2">FIM</h5>
<div class="row grid-responsive">
	<div class="column ">
		<div class="card">
			<div class="card-title">
				<h3>Статистика по FIM и инструкциям</h3>
			</div>
			<div class="card-block">
				<table>
					<thead>
						<tr>
							<th>Название FIM</th>
							<th>Инструкция</th>
							<th>Количество срабатываний</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($data as $que)
						<tr>
							<td>{{$que->fim_id}}</td>
							<td>{{$que->name}}</td>
							<td>{{$que->stat}}</td>
						</tr>
					@endforeach
					</tbody>
				</table>
			</div>
		</div>
	</div>
</div>