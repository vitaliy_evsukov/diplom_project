<h5 class="mt-2">FIM</h5>
<div class="row grid-responsive">
	<div class="column ">
		<div class="card">
			<div class="card-title">
				<h3>FIM</h3>
			</div>
			<div class="card-block">
				<table>
					<thead>
						<tr>
							<th>ID</th>
							<th>FIM</th>
							<th>Путь</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>PMC-S1000DBIKE-B6865-EPWG1-00_001-00_en-US</td>
							<td>http://localhost/static/PMC-S1000DBIKE-B6865-EPWG1-00_001-00_en-US.xml</td>
						</tr>
					</tbody>
				</table>
                <div class="column">
					
					<a class="button" href="/fim1">Просмотр обычного FIM</a>
					
					
					<a class="button" href="/fim2">Видоизмененный FIM</a>
					
					
				</div> 
			</div>
		</div>
	</div>
</div>