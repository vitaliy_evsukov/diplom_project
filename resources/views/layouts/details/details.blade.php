<div class="row grid-responsive">
	<div class="column ">
		<div class="card">
			<div class="card-title">
				<h3>Список деталей</h3>
			</div>
			<div class="card-block">
				<table>
					<thead>
						<tr>
							<th>Деталь</th>
							<th>Цена в долларах</th>
							<th>Действие</th>
						</tr>
					</thead>
					<tbody>
					@foreach ($data as $que)
						<tr>
							<td>{{$que->name}}</td>
							<td>{{$que->cost}}</td>
							<td><a class="button" href="/diplom_project/public/details/edit/id?id={{$que->id}}">Редактировать</a><td>
                        </tr>
                    @endforeach
					</tbody>
				</table>
				<div class="column">
		<!-- Default Button -->
		<a class="button" href="/diplom_project/public/details/create">Добавить деталь</a>
				</div>
			</div>
		</div>
	</div>
</div>