<div class="navbar">
    <div class="row">
        <div class="column column-30 col-site-title"><a href="/diplom_project/public/index/" class="site-title float-left">ПАО "ИРКУТ"</a></div>
        <div class="column column-40 col-search">
        </div>
        <div class="column column-30">
            <div class="user-section">
                <a href="#">
                <div class="username">
                    <h4>{{ Auth::user()->name }}</h4>
                    <p>Administrator</p>
                </div>
                </a>
            </div>
        </div>
    </div>
</div>