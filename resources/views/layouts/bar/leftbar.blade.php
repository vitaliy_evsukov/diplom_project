<div id="sidebar" class="column">
    <h5>Воздушные судна</h5>
    <ul>
        <li><a href="/diplom_project/public/airplane/">Список самолётов</a></li>
    </ul>
    <h5>Статистика</h5>
    <ul>
        <li><a href="/diplom_project/public/indexstat/">Статистика по FIM и инструкциям</a></li>
    </ul>
    <h5>Администрирование</h5>
    <ul>
        <li><a href="/diplom_project/public/company/">Компании</a></li>
        <li><a href="#">Пользователи</a></li>
        <li><a href="/diplom_project/public/fim/">FIM</a></li>
        <li><a href="/diplom_project/public/details/">Детали</a></li>
    </ul>
    <ul>
        <li><div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                                     Выход
                                        
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div></li>
    </ul>
</div>