<div class="row grid-responsive">
	<div class="column ">
		<div class="card">
			<div class="card-title">
				<h3>Список компаний</h3>
			</div>
			<div class="card-block">
				<table>
					<thead>
						<tr>
							<th>Компания</th>
						</tr>
					</thead>
					<tbody>
                    @foreach ($data as $que)
						<tr>
							<td>{{$que->name}}</td>
							<td><a class="button" href="/diplom_project/public/company/edit/id?id={{$que->id}}">Редактировать</a><td>
                        </tr>
                    @endforeach
					</tbody>
				</table>
				<div class="column">
		<!-- Default Button -->
		<a class="button" href="/diplom_project/public/company/create">Добавить компанию</a>
				</div>
			</div>
		</div>
	</div>
</div>