<div class="row grid-responsive">
	<div class="column ">
		<div class="card">
			<div class="card-title">
				<h3>Список деталей</h3>
			</div>
			<div class="card-block">
				<table>
					<thead>
						<tr>
							<th>№</th>
							<th>Имя</th>
							<th>Фамилий</th>
                            <th>Почтовый ящик</th>
                            <td>Роль</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>1</td>
							<td>Имя</td>
                            <td>Фамилия</td>
                            <td>adsfgw@gmail.com</td>
                            <td>Администратор</td>
							<td><a class="button" href="#">Редактировать</a><td>
						</tr>
					</tbody>
				</table>
				<div class="column">
		<!-- Default Button -->
		<a class="button" href="#">Добавить деталь</a>
				</div>
			</div>
		</div>
	</div>
</div>