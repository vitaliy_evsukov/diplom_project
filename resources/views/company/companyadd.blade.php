<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<meta name="viewport" content="width=1280, initial-scale=1, maximum-scale=1, user-scalable=no">
	<title>Система статистики</title>
	
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700" rel="stylesheet">
	
	<!-- Template Styles -->
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
	
	<!-- CSS Reset -->
	<link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
	
	<!-- Milligram CSS minified -->
	<link rel="stylesheet" href="{{ asset('css/milligram.min.css') }}">
	
	<!-- Main Styles -->
	<link rel="stylesheet" href="{{ asset('css/styles.css') }}">
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>

<body>
@include('layouts.bar.topbar')
	<div class="row">
	@include('layouts.bar.leftbar')	
		<section id="main-content" class="column column-offset-20">
        @include('layouts.company.companyadd')

        <script src="{{ asset('js/chart.min.js') }}"></script>
	<script src="{{ asset('js/chart-data.js') }}"></script>
	

</body>
</html> 