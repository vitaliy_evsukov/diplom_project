<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<title>Система статистики</title>
	<meta name="viewport" content="width=1280, initial-scale=1, maximum-scale=1, user-scalable=no">
	
	<!-- Google Fonts -->
	<link href="https://fonts.googleapis.com/css?family=Montserrat:300,300i,400,400i,500,500i,600,600i,700" rel="stylesheet">
	
	<!-- Template Styles -->
	<link rel="stylesheet" href="{{ asset('css/font-awesome.min.css') }}">
	
	<!-- CSS Reset -->
	<link rel="stylesheet" href="{{ asset('css/normalize.css') }}">
	
	<!-- Milligram CSS minified -->
	<link rel="stylesheet" href="{{ asset('css/milligram.min.css') }}">
	
	<!-- Main Styles -->
	<link rel="stylesheet" href="{{ asset('css/styles.css') }}">
	
	<!--[if lt IE 9]>
	<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->
</head>

<body>
	@include('layouts.bar.topbar')
	<div class="row">
	@include('layouts.bar.leftbar')	
		<section id="main-content" class="column column-offset-20">
		@include('layouts.startblock')		
			<!--Forms-->
		<!--	<h5 class="mt-2">Forms</h5><a class="anchor" name="forms"></a>
			<div class="row grid-responsive">
				<div class="column ">
					<div class="card">
						<div class="card-title">
							<h3>Sample Form</h3>
						</div>
						<div class="card-block">
							<form>
								<fieldset>
									<label for="nameField">Name</label>
									<input type="text" placeholder="Jane Donovan" id="nameField">
									<label for="ageRangeField">Age Range</label>
									<select id="ageRangeField">
										<option value="0-13">0-13</option>
										<option value="14-17">14-17</option>
										<option value="18-23">18-23</option>
										<option value="24+">24+</option>
									</select>
									<label for="commentField">Comment</label>
									<textarea placeholder="Hi Jane…" id="commentField"></textarea>
									<div class="float-right">
										<input type="checkbox" id="confirmField">
										<label class="label-inline" for="confirmField">Send a copy to yourself</label>
									</div>
									<input class="button-primary" type="submit" value="Send">
								</fieldset>
							</form>
						</div>
					</div>
				</div>
			</div>
-->
			<!--Buttons-->
			<!-- <h5 class="mt-2">Buttons</h5><a class="anchor" name="buttons"></a>
			<div class="row grid-responsive">
				<div class="column">
					
					<a class="button" href="#">Default Button</a>
					
					
					<button class="button button-outline">Outlined Button</button>
					
					
					<input class="button button-clear" type="submit" value="Clear Button">
				</div>
			</div> -->
			
			<!--Tables-->
			<!-- <h5 class="mt-2">Tables</h5><a class="anchor" name="tables"></a>
			<div class="row grid-responsive">
				<div class="column ">
					<div class="card">
						<div class="card-title">
							<h3>Current Members</h3>
						</div>
						<div class="card-block">
							<table>
								<thead>
									<tr>
										<th>Name</th>
										<th>Role</th>
										<th>Age</th>
										<th>Location</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>Jane Donovan</td>
										<td>UI Developer</td>
										<td>23</td>
										<td>Philadelphia, PA</td>
									</tr>
									<tr>
										<td>Jonathan Smith</td>
										<td>Designer</td>
										<td>30</td>
										<td>London, UK</td>
									</tr>
									<tr>
										<td>Kelly Johnson</td>
										<td>UX Developer</td>
										<td>25</td>
										<td>Los Angeles, CA</td>
									</tr>
									<tr>
										<td>Sam Davidson</td>
										<td>Programmer</td>
										<td>28</td>
										<td>Philadelphia, PA</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div> -->
	
	<script src="{{ asset('js/chart.min.js') }}"></script>
	<script src="{{ asset('js/chart-data.js') }}"></script>
	

</body>
</html> 