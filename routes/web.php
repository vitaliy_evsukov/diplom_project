<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/login');
});

Route::get('/index', 'mainController@index');
Route::get('/company', 'CompanyController@index');
Route::get('/company/create', 'CompanyControllerAdd@index');
Route::post('/company/create', 'CompanyControllerAdd@store');
Route::get('/company/edit/{id}', 'CompanyControllerEdit@edit');
Route::post('/company/edit/{id}', 'CompanyControllerEdit@update');
Route::post('/company/delete/{id}', 'CompanyControllerEdit@destroy');
Route::post('/company/zaglushka', 'CompanyControllerAdd@create');

Route::get('/airplane', 'AirplaneController@index');
Route::get('/airplane/create', 'AirplaneControllerAdd@index');
Route::post('/airplane/create', 'AirplaneControllerAdd@store');
Route::get('/airplane/edit/{id}', 'AirplaneControllerEdit@edit');
Route::post('/airplane/edit/{id}', 'AirplaneControllerEdit@update');
Route::post('/airplane/delete/{id}', 'AirplaneControllerEdit@destroy');
Route::post('/airplane/zaglushka', 'AirplaneControllerAdd@create');

Route::get('/details', 'DetailsController@index');
Route::get('/details/create', 'DetailsControllerAdd@index');
Route::post('/details/create', 'DetailsControllerAdd@store');
Route::get('/details/edit/{id}', 'DetailsControllerEdit@edit');
Route::post('/details/edit/{id}', 'DetailsControllerEdit@update');
Route::post('/details/delete/{id}', 'DetailsControllerEdit@destroy');
Route::post('/details/zaglushka', 'DetailsControllerAdd@create');
Auth::routes();

Route::get('/fim', 'FIMController@index');
Route::get('/fim/observe1/{id}', 'fim1Controller@index');
Route::get('/fim/observe2/{id}', 'fim2Controller@index');
Route::get('/fim/observe2/sub/{id}', 'fim2Controller@update');
Route::get('/indexstat', 'Fimstats@index');

Route::get('/fimadd', 'FIMControllerAdd@index');
Route::post('/fimadd', 'FIMControllerAdd@fileUpload');

