<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\indexfim;
use App\FIM;

class fim2Controller extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        {
        
            $data = indexfim::Where('fim_id', $_GET['id'])->get()->sortByDesc("stat");
            $fim = FIM::all();
            foreach($data as $i)
            {
                foreach($fim as $a)
                if($a['id'] == $i['fim_id'])
                {
                    $title=$a['name'];
                }
            }
            return view('fim1', compact('data','title'));
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update()
    {
        $id=$_GET['indexfim_id'];
        $data = indexfim::Where('id', $id)->get();//->update($request->all('id','name','cost','time'));
        foreach($data as $i)
        {   
            
            $i['stat'] = $i['stat'] + 1;
            $resp['id']=$i['id'];
            $resp['description']=$i['description'];
            $resp['name']=$i['name'];
            $resp['stat']=$i['stat'];
        }
        indexfim::whereId($id)->update($resp);
        
        return redirect('fim');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
