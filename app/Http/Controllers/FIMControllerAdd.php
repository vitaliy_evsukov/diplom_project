<?php

namespace App\Http\Controllers;
use Orchestra\Parser\Xml\Facade as XmlParser;

use Illuminate\Http\Request;
use App\FIM;
use App\indexfim;

class FIMControllerAdd extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('fimadd');
    }
    
    public function fileUpload(Request $request){

        if($request->isMethod('post')){
    
            if($request->hasFile('fim')) {
                $file = $request->file('fim');
                $file->move(storage_path('fim'), $file->getClientOriginalName());
                $xml = XmlParser::load(storage_path('fim').'/'.$file->getClientOriginalName());
                $fim = $xml->parse([
                'namefim' => ['uses' => 'instruction::name'],
                'instruction' => ['uses' => 'instruction.index[name,description]'],
                
            ]);
            $input1['name'] = $fim['namefim'];
            $input1['adress'] = storage_path('fim').'/'.$file->getClientOriginalName();
            FIM::create($input1);
            }
            
         }

         $inputclear = array();
         foreach ($fim['instruction'] as $key){
            $input2 = $inputclear;
            $input2['name'] = $key['name'];
            $input2['description'] =$key['description'];
            $need = FIM::where('name', $fim['namefim'])->take(1)->get();
            foreach ($need as $que){
            $input2['fim_id'] = $que['id'] ;
            indexfim::create($input2);
            }
         };
         return redirect('fim');

       }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
