<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Companys extends Model
{
    public function airplanes()
    {
        return $this->hasMany('App\Airplane', 'name');
    }
    protected $table = 'companies';

}