<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Airplane extends Model
{
    public function company()
    {
        return $this->hasOne('App\Companys', 'company_id', 'name'  );
    }
    protected $table = 'airplane';
    protected $fillable = ['series','number','company_id'];
}
