<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class indexfim extends Model
{
    protected $table = 'indexfim';
    protected $fillable = ['id','name','description','fim_id','stat'];
}
