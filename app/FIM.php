<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FIM extends Model
{
    protected $table = 'fim';
    protected $fillable = ['name','adress'];
}
